package com.neshke.minihealthapp.loginsignup

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.neshke.minihealthapp.R
import com.neshke.minihealthapp.ui.ProfileActivity
import kotlinx.android.synthetic.main.activity_login_sign_up.*

class LoginSIgnUpActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.hide()
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN

        setContentView(R.layout.activity_login_sign_up)
        auth = FirebaseAuth.getInstance()



        button_signup.setOnClickListener {
            signUp()

        }
        button_login.setOnClickListener {
            login()
        }


    }

    //Provera da li je korisnik vec predhodno ulogovan
    public override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        updateUI(currentUser)
    }

    private fun signUp() {

        if(edit_text_email.text.toString().isEmpty()){
            edit_text_email.error = "Please enter email"
            edit_text_email.requestFocus()
            return
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(edit_text_email.text.toString()).matches()){
            edit_text_email.error = "Please enter valid email"
            edit_text_email.requestFocus()
            return
        }

        if(edit_text_pwd.text.toString().isEmpty()){
            edit_text_pwd.error = "Please enter password"
            edit_text_pwd.requestFocus()
            return
        }

        if(edit_text_pwd.text.length < 6){
            edit_text_pwd.error = "Password must be at least 6 characters"
            edit_text_pwd.requestFocus()
            return
        }

        auth.createUserWithEmailAndPassword(edit_text_email.text.toString(), edit_text_pwd.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Toast.makeText(baseContext, "User created successfully!",
                        Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this, ProfileActivity::class.java))
                    finish()
                } else {
                    println(task.exception)
                    Toast.makeText(baseContext, "Sign Up failed. Try again after some time",
                        Toast.LENGTH_SHORT).show()
                }
            }
    }

    private fun login() {
        //Provera da li su akreditivi uneti i da li su ispravno uneti
        if (edit_text_email.text.toString().isEmpty()) {
            edit_text_email.error = "Please enter email"
            edit_text_email.requestFocus()
            return
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(edit_text_email.text.toString()).matches()) {
            edit_text_email.error = "Please enter valid email"
            edit_text_email.requestFocus()
            return
        }

        if (edit_text_pwd.text.toString().isEmpty()) {
            edit_text_pwd.error = "Please enter password"
            edit_text_pwd.requestFocus()
            return
        }


// Metoda za prijavu korisnika na firebase
        auth.signInWithEmailAndPassword(edit_text_email.text.toString(), edit_text_pwd.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val user = auth.currentUser
                    Toast.makeText(
                        baseContext, "Login successfull!",
                        Toast.LENGTH_SHORT
                    ).show()
                    updateUI(user)
                } else {
                    println(task.exception)
                    updateUI(null)
                }
            }
    }


    private fun updateUI(currentUser: FirebaseUser?) {

        if (currentUser != null) {
                startActivity(Intent(this, ProfileActivity::class.java))
            Toast.makeText(
                    baseContext, "Login successfull!",
                    Toast.LENGTH_SHORT
            ).show()
                finish()
        } else {
            Toast.makeText(
                baseContext, "There is no user with Email or Password is incorrect ",
                Toast.LENGTH_SHORT
            ).show()
        }
    }


}