package com.neshke.minihealthapp.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.neshke.minihealthapp.R
import com.neshke.minihealthapp.loginsignup.LoginSIgnUpActivity
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        val stepCounterFragment = StepCounterFragment()
        val drinkOMeterFragment = DrinkOMeterFragment()

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().apply {

                replace(R.id.fragment_layout, StepCounterFragment.newInstance())
                commitNow()
            }
        }

        button_step_counter.setOnClickListener {
            supportFragmentManager.beginTransaction().apply {

                replace(R.id.fragment_layout, StepCounterFragment.newInstance())
                addToBackStack(null)
                commit()
            }
        }

        button_drink_o_meter.setOnClickListener {
            supportFragmentManager.beginTransaction().apply {

                replace(R.id.fragment_layout, drinkOMeterFragment)
                addToBackStack(null)
                commit()
            }
        }

        button_profile.setOnClickListener{

        }


        supportActionBar?.hide()
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN



        auth = FirebaseAuth.getInstance()
        button_logout.setOnClickListener {
            auth.signOut()
            startActivity(Intent(this, LoginSIgnUpActivity::class.java))
            finish()

        }
    }
}