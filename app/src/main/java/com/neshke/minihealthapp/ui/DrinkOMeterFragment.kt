package com.neshke.minihealthapp.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.neshke.minihealthapp.R
import kotlinx.android.synthetic.main.fragment_drink_o_meter.*
import java.time.format.DateTimeFormatter
import java.util.*

class DrinkOMeterFragment : Fragment() {

    private lateinit var viewModel: DrinkOMeterViewModel

    private var day = 0
    private var month = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_drink_o_meter, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get( DrinkOMeterViewModel::class.java)

        tv_daily_number.text = viewModel.savedDayNumber.toString()
        tv_monthly_number.text = viewModel.savedMonthNumber.toString()
        tv_total_number.text = viewModel.savedNumber.toString()

        button_drink.setOnClickListener{
            countDrink()
        }
    }

    fun countDrink(){
        viewModel.addNumber()
        val cal = Calendar.getInstance()
        viewModel.saveDate(cal.get(Calendar.DAY_OF_MONTH).toInt(),cal.get(Calendar.MONTH).toInt())
        viewModel.saveDrinks(cal.get(Calendar.DAY_OF_MONTH).toInt(),cal.get(Calendar.MONTH).toInt())

        tv_daily_number.text = viewModel.savedDayNumber.toString()
        tv_monthly_number.text = viewModel.savedMonthNumber.toString()
        tv_total_number.text = viewModel.savedNumber.toString()

        viewModel.checkDayMonth(cal.get(Calendar.DAY_OF_MONTH).toInt(),cal.get(Calendar.MONTH).toInt())
    }



}