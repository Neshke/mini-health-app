package com.neshke.minihealthapp.ui

import androidx.lifecycle.ViewModel
import java.util.*

class DrinkOMeterViewModel: ViewModel() {

    var number = 0
    var savedDay: Int = 0
    var savedDayNumber: Int = 0
    var savedMonth: Int = 0
    var savedMonthNumber: Int = 0
    var savedNumber: Int = 0

    fun addNumber(){
        number++
    }

    fun saveDate(currentDay: Int, currentMonth: Int){
            savedDay = currentDay
            savedMonth = currentMonth
    }

    fun saveDrinks(currentDay: Int, currentMonth: Int){
        savedDayNumber = number
        savedMonthNumber = number
    }
    fun checkDayMonth(currentDay: Int, currentMonth: Int){
        if(currentDay != savedDay){
            savedNumber = savedMonthNumber
            savedDayNumber = 0
        }
        if(currentMonth != savedMonth){
            savedNumber = savedMonthNumber
            savedMonthNumber = 0
        }
    }
}